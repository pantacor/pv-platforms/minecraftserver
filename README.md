# Pantahub enabled minecraft server

Docker image to run minecraft java server on pantahub enabled devices such as Raspberry Pi 3 and Raspberry Pi 4.
Based on githubusercontent.com/zuzkins/docker-images.

## Supported architecures
- ARM32v6
- ARM64v8
- AMD64

## Features
- Downloads the latest stable minecraft server version at image build time
- Or you can specify an older version to download at build time
- Runs the specified server version
- Customizable settings by directly using environment variables or creating the file `/env.json`

## How to build
`docker build . -f Dockerfile.ARCH [--build-arg MINECRAFT_VERSION=1.6.15]`

By default, the dockerfile will try to download the latest stable official minecraft server jar file.
If you want to build a specific Version, you can use the build argument MINECRAFT_VERSION.

## Settings
If the user does not specify a setting via `/env.json`, the server uses the following default minecraft server settings. You can find information about the server settings here: https://minecraft.gamepedia.com/Server.properties#Java_Edition_3

To override a default setting, add an entry to `/env.json`:
```
{
        "pvp": false
}
```

### Notes
You need to replace `.` (dots) and `-` (dashes) in the setting names with underscores.
So, instead of using `level-name`, you need to use `level_name` as a key in `/env.json`:
```
{
        "pvp": false,
        "level_name": "My cool world"
}
```

### Setting OPs
To have Notch and Guss as OPs:
```
{
        "ops": [Guss, Notch]
}
```

### Java Arguments
Java arguments can be set with `JAVA_OPTS`:
```
{
        "JAVA_OPTS": "-Xmx4096 -Xms2048"
}
```