#! /bin/bash

# Source: https://raw.githubusercontent.com/zuzkins/docker-images/master/minecraft/minecraft.sh

echo eula=true > /data/eula.txt

# set default server RAM to 768MB
# user can change it in pantacor one UI
JAVA_OPTS="-Xmx768M -Xms768M"
pvmeta update "minecraft.version=$(cat /version)"
echo "Starting Server with version $MC_VERSION"

pvmeta update "minecraft.version=$(cat /version)"
echo "Starting Server with version $MC_VERSION"
. /opt/json2env.sh /env.json
/opt/update_properties.sh
echo ""
echo "Starting Server with server settings:"
cat /data/server.properties
echo ""
echo "Operators:"
cat /data/ops.json
echo ""
echo "Whiltelist:"
cat /data/whitelist.json
java $JAVA_OPTS -jar /opt/minecraft/server.jar &
pid="$!"
trap "echo 'Stopping PID $pid'; kill -SIGTERM $pid" SIGINT SIGTERM


# A signal emitted while waiting will make the wait command return code > 128
# Let's wrap it in a loop that doesn't end before the process is indeed stopped
while kill -0 $pid > /dev/null 2>&1; do
    wait
done
