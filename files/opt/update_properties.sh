#!/bin/bash

# load properties
basedir=$(dirname "$0")
settings_file="/data/server.properties"
settings=$(cat "$basedir/server.properties.default")

rm -f "$settings_file"

echo "$settings" | while read line; do
        key=${line%=*}
        if [ "$key" == "" ] || [ "$key" == "$line" ]; then
                continue
        fi
        value=${line#*=}
        env_value_var_name=$(echo "$key" | tr '-' '_' | tr '.' '_')
        env_value="${!env_value_var_name}"
        # check unset
        if [[ -z  ${!env_value_var_name+set} ]]; then
                a=a
        elif [[ -n  ${env_value} ]]; then # set but can be empty
                echo "Found env variable $key ($env_value_var_name) with value ${env_value}"
                value="${env_value}"
        
        fi
        printf "%s=%s\n" "$key" "$value" >> "$settings_file"
done

# update OPs list
# partly taken from https://msftstack.wordpress.com/2015/09/28/bash-script-to-convert-a-minecraft-username-to-uuid/
UUID_URL=https://api.mojang.com/users/profiles/minecraft
echo "[" > "/data/ops.json"

if [[ -z ${op_permission_level} ]]; then
        op_permission_level=4
fi

for op in $(echo "$ops" | sed "s/,/ /g")
do
        mojang_output=$(wget -qO- "$UUID_URL/$op")
        rawUUID=$(echo "$mojang_output" | jq -r '.id')
        UUID=${rawUUID:0:8}-${rawUUID:8:4}-${rawUUID:12:4}-${rawUUID:16:4}-${rawUUID:20:12}
        {
                if [[ -z "$not_first" ]]; then
                        not_first=yes
                else
                        echo "  ,"
                fi
                echo "  {"
                echo "    \"uuid\": \"${UUID}\","
                echo "    \"name\": \"${op}\","
                echo "    \"level\": ${op_permission_level}"
                echo "  }"
        } >> "/data/ops.json"
done
echo "]" >> "/data/ops.json"

echo "[" > "/data/whitelist.json"

for player in $(echo "$white_list_list" | sed "s/,/ /g")
do
        mojang_output=$(wget -qO- "$UUID_URL/$player")
        rawUUID=$(echo "$mojang_output" | jq -r '.id')
        UUID=${rawUUID:0:8}-${rawUUID:8:4}-${rawUUID:12:4}-${rawUUID:16:4}-${rawUUID:20:12}
        {
                if [[ -z "$not_first_w" ]]; then
                        not_first_w=yes
                else
                        echo "  ,"
                fi
                echo "  {"
                echo "    \"uuid\": \"${UUID}\","
                echo "    \"name\": \"${player}\","
                echo "  }"
        } >> "/data/whitelist.json"
done
echo "]" >> "/data/whitelist.json"