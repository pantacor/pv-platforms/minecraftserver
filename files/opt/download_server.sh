#!/bin/bash

if [[ $# -le 1 ]]; then
        echo "No version specified, fetching latest verions"
        MC_VERSION=$(curl -s -k https://launchermeta.mojang.com/mc/game/version_manifest.json | jq -r '.latest.release')
        echo "Fetched version ${MC_VERSION}"
else
        MC_VERSION=$1
        echo "Version $MC_VERSION specified"
fi

version_json_url=$(curl -s -k https://launchermeta.mojang.com/mc/game/version_manifest.json | jq -r ".versions[] | select(.id==\"$MC_VERSION\").url")

if [ -z "$version_json_url" ]; then
        echo "Error: Could not find minecraft version in mojang release manifest"
	echo "$(curl -s -k https://launchermeta.mojang.com/mc/game/version_manifest.json) asd"
        exit 1
fi

echo "Found json ${version_json_url} to version ${MC_VERSION}"

download_url=$(curl -s -k "$version_json_url" | jq -r '.downloads.server.url')


if [ -z "$download_url" ]; then
        echo "Error: Could not find download link to version $MC_VERSION"
        exit 1
fi

echo "Downloading server.jar from $download_url..."
wget "$download_url" -O /opt/minecraft/server.jar

echo "$MC_VERSION" > /version
