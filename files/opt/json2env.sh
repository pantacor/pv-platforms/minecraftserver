#!/bin/sh
export_envvar()
{
	key=$1
	value=$2

	if [ -n "$key" ]; then
		export "$key=$value"
		echo "export \"$key=$value\""
		return 0
	else
		return 1
	fi
}

old_key=""
old_value=""
basedir=$(dirname "$0")


if ! [ -f "$basedir/JSON.sh" ]; then
	echo "Error: JSON.sh not found in base directory: $basedir"
	exit 1
fi

jsonsh=$basedir/JSON.sh


old_key=""
old_value=""

while [ $# -ge 1 ]; do
	if ! [ -f "$1" ]; then
		echo "Environment json file \"$1\" not found!"

		shift 1
		continue
	fi

	file=$1
	key=""
	value=""
	json=$($jsonsh -l < "$file")

	while IFS= read -r line
	do
		# Removing whitespaces and tabs at beginning
		# JSON.sh seems to insert those sometimes...
		line=$(echo "$line" | sed 's/^[ \t]*//')
		key=$(echo "$line"| cut -d '"' -f2)

		value=$(echo "$line"| cut -f2)
		case $value in
			\"*\")
				value=${value%?}
				value=${value#?}
				;;
			* )
				# numbers and such, keep 'em
				;;
		esac
		if [ "$key" = "$old_key" ]; then
			value="$old_value,$value"
			old_value="$value"
		else
			old_value="$value"
			old_key="$key"
		fi
		export_envvar "$key" "$value"
	done <<EOF
	$json
EOF
	
	shift 1
done
